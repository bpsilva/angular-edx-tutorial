app.directive('isolatedScopeDirective',  function () {
    return {
        scope: {
            author: '=author'
        },
        controller: ['$scope',function($scope)
        {
          console.log($scope.author);  
        }],
        replace: true,
        restrict: 'EA',
        template:
        '<div>{{author}}<input  ng-model=author></input></div>'
    }
})