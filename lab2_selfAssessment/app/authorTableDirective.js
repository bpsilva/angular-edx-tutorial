app.directive('authorTable', function () {
    return {
        scope:{
            author: '='
        },
        replace: true,
        restrict: 'EA',
        template: 
        ' <tr><td>{{author.name}}</td><td>{{author.nationality}}</td><td>{{author.dates}}</td></tr>'
    }
})