app.controller('LabController', [
    function () {
        var vm = this;
        vm.authors = [
            {
                name: 'Mark Twain',
                nationality: 'American',
                dates: '1835-1910'
            },
            {
                name: 'Machado de Assis',
                nationality: 'Brazilian',
                dates: '1839-1908'
            }        
        ]
    }
]);