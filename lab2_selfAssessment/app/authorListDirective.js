app.directive('authorList',  function () {
    return {
        scope: {
            author: '@'
        },
        controller: ['$scope',function($scope)
        {
          console.log($scope.author);  
        }],
        replace: true,
        restrict: 'EA',
        template:
        '<li>{{author}}</li>'
    }
})