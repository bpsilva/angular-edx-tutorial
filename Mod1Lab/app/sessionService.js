app.service('sessionService', ['$window','formattingFactory',

    function sessionService($window,formattingFactory) {
        this.save = save;
        this.get = get;
        this.clear = clear;
        function save(key, value) {
            if(key=='name')
            {
                value = formattingFactory.format(value);
            }
            $window.sessionStorage.setItem(key, value);
        }
        function get(key) {
            return $window.sessionStorage.getItem(key);
        }
        function clear() {
            $window.sessionStorage.clear();
        }

    }
]);

